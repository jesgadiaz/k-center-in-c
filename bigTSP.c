/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Gon, Gon+, HS, CDSh, and CDSh+
 * for the big instances from TSP-Lib (sweden, burma, china)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>

/*
 * 
 */

const char* str1;
const char* algorithm;

int main(int argc, char** argv) {
    algorithm = argv[1];
    int seed = atoi(argv[3]);
    int n, m, k;
    double total_time = 0;
    double exec_time[4];
    float vertices_x_y[71009][2];
    float greatest_cost;
    printf("Experimental approximation factor \n");
    for(int instance=0;instance<4;instance++){
        // LOAD INSTANCE
        k = (instance + 1)*25;
        str1 = argv[2];
        if(instance==0){
            int i = 0;
           float cost;
           FILE* file = fopen(str1, "r");
           char line[50];
           if (file == NULL){
               printf("FILE NOT FOUND");
           }else{
               int v;
               float x, y;
               while(fgets(line, sizeof(line), file)) {
                   int j = 0;
                   char* token = strtok(line, " ");
                   while (token) {
                       if(i == 0 && j == 0){
                           n = atoi(token);
                       }
                       if(i > 0 && j == 0){
                           v = atoi(token);
                       }
                       if(i > 0 && j == 1){
                           x = atof(token);
                       }
                       if(i > 0 && j == 2){
                           y = atof(token);
                           vertices_x_y[v-1][0] = x;
                           vertices_x_y[v-1][1] = y;
                       }
                       token = strtok(NULL, " ");
                       j++;
                   }
                   free(token);
                   i++;
                }
                fclose(file);
                if(strcmp(algorithm, "HS") == 0 || strcmp(algorithm, "CDS") == 0
                    || strcmp(algorithm, "CDSh") == 0 || strcmp(algorithm, "CDSh+") == 0
                    || strcmp(algorithm, "HS+") == 0){
                   float d, d1, d2;
                    for(int i=0;i<n;i++){
                        for(int j=0;j<n;j++){
                            d1 = vertices_x_y[i][0] - vertices_x_y[j][0];
                            d2 = vertices_x_y[i][1] - vertices_x_y[j][1];
                            d = d1*d1 + d2*d2;
                            //d = sqrt( pow(vertices_x_y[i][0] - vertices_x_y[j][0],2) 
                            //              + pow(vertices_x_y[i][1] - vertices_x_y[j][1],2) );
                           if(d > greatest_cost) {
                               greatest_cost = d;
                           }
                        }
                    }
                   greatest_cost = sqrt(greatest_cost);
                }
           }   
        }
        
        clock_t begin = clock();
        int C[k];
        
        // GON
        if(strcmp(algorithm, "Gon") == 0){
            srand(seed);
            C[0] = rand() % n;
            float distance[n];
            float max_dist;
            float d, d1, d2;
            for(int i=0;i<n;i++){
                distance[i] = +INFINITY;
            }
            for(int i=1;i<k;i++){
                // UPDATE DISTANCE
                for(int j=0;j<n;j++){
                    d1 = vertices_x_y[C[i-1]][0] - vertices_x_y[j][0];
                    d2 = vertices_x_y[C[i-1]][1] - vertices_x_y[j][1];
                    d = d1*d1 + d2*d2;
                    //d = sqrt( pow(vertices_x_y[C[i-1]][0] - vertices_x_y[j][0],2) 
                    //    + pow(vertices_x_y[C[i-1]][1] - vertices_x_y[j][1],2) );
                    if(d < distance[j]){
                        distance[j] = d;
                    }
                }
                // SEARCH FOR FARTHEST VERTEX
                float max_dist = 0;
                int farthest_vertex = -1;
                for(int j=0;j<n;j++){
                    if(distance[j] > max_dist){
                        max_dist = distance[j];
                        farthest_vertex = j;
                    }
                }
                C[i] = farthest_vertex;
            }
            // UPDATE DISTANCE AGAIN
            for(int i=0;i<n;i++){
                d1 = vertices_x_y[C[k-1]][0] - vertices_x_y[i][0];
                d2 = vertices_x_y[C[k-1]][1] - vertices_x_y[i][1];
                d = d1*d1 + d2*d2;
                //d = sqrt( pow(vertices_x_y[C[k-1]][0] - vertices_x_y[i][0],2) 
                //        + pow(vertices_x_y[C[k-1]][1] - vertices_x_y[i][1],2) );
                if(d < distance[i]){
                    distance[i] = d;
                }
            }
            max_dist = 0;
            for(int i=0;i<n;i++){
                if(distance[i] > max_dist){
                    max_dist = distance[i];
                }
            }
            //fflush(stdout);
            printf("%f \n", sqrt(max_dist));
        }

        // GON+
        if(strcmp(algorithm, "Gon+") == 0){
            float best_size = +INFINITY;
            float distance[n];
            float max_dist;
            float d, d1, d2;
            for(int p=0;p<n;p++){
                C[0] = p;
                for(int i=0;i<n;i++){
                    distance[i] = +INFINITY;
                }
                for(int i=1;i<k;i++){
                    // UPDATE DISTANCE
                    for(int j=0;j<n;j++){
                        d1 = vertices_x_y[C[i-1]][0] - vertices_x_y[j][0];
                        d2 = vertices_x_y[C[i-1]][1] - vertices_x_y[j][1];
                        d = d1*d1 + d2*d2;
                        //d = sqrt( pow(vertices_x_y[C[i-1]][0] - vertices_x_y[j][0],2) 
                        //    + pow(vertices_x_y[C[i-1]][1] - vertices_x_y[j][1],2) );
                        if(d < distance[j]){
                            distance[j] = d;
                        }
                    }
                    // SEARCH FOR FARTHEST VERTEX
                    float max_dist = 0;
                    int farthest_vertex = -1;
                    for(int j=0;j<n;j++){
                        if(distance[j] > max_dist){
                            max_dist = distance[j];
                            farthest_vertex = j;
                        }
                    }
                    C[i] = farthest_vertex;
                }
                // UPDATE DISTANCE AGAIN
                for(int i=0;i<n;i++){
                    d1 = vertices_x_y[C[k-1]][0] - vertices_x_y[i][0];
                    d2 = vertices_x_y[C[k-1]][1] - vertices_x_y[i][1];
                    d = d1*d1 + d2*d2;
                    //d = sqrt( pow(vertices_x_y[C[k-1]][0] - vertices_x_y[i][0],2) 
                    //    + pow(vertices_x_y[C[k-1]][1] - vertices_x_y[i][1],2) );
                    if(d < distance[i]){
                        distance[i] = d;
                    }
                }
                max_dist = 0;
                for(int i=0;i<n;i++){
                    if(distance[i] > max_dist){
                        max_dist = distance[i];
                    }
                }
                if(max_dist < best_size){
                    best_size = max_dist;
                }
            }
            printf("%f \n", sqrt(best_size));
        }
        
        // HS
        if(strcmp(algorithm, "HS") == 0){
            float high = greatest_cost;
            float low = 0;
            float mid;
            float d, d1, d2;
            srand(seed);
            float distance[n];
            bool stop_condition = false;
            float best_size = +INFINITY;
            int iteration = 0;
            while(!stop_condition){
                mid = (high + low) / 2;
                C[0] = rand() % n;
                for(int i=0;i<n;i++){
                    distance[i] = +INFINITY;
                }
                for(int i=1;i<k;i++){
                    // UPDATE DISTANCE
                    for(int j=0;j<n;j++){
                        d1 = vertices_x_y[C[i-1]][0] - vertices_x_y[j][0];
                        d2 = vertices_x_y[C[i-1]][1] - vertices_x_y[j][1];
                        d = d1*d1 + d2*d2;
                        //d = sqrt( pow(vertices_x_y[C[i-1]][0] - vertices_x_y[j][0],2) 
                        //    + pow(vertices_x_y[C[i-1]][1] - vertices_x_y[j][1],2) );
                        if(d < distance[j]){
                            distance[j] = d;
                        }
                    }
                    // SEARCH FOR NEXT VERTEX (2r)
                    int center = -1;
                    for(int j=0;j<n;j++){
                        if(distance[j] > (2*mid)*(2*mid)){
                            center = j;
                        }
                    }
                    if(center != -1){
                        C[i] = center;   
                    }else{
                        C[i] = rand() % n;
                    }
                }
                // RE-UPDATE DISTANCE
                float max_dist = 0;
                for(int j=0;j<n;j++){
                    d1 = vertices_x_y[C[k-1]][0] - vertices_x_y[j][0];
                    d2 = vertices_x_y[C[k-1]][1] - vertices_x_y[j][1];
                    d = d1*d1 + d2*d2;
                    //d = sqrt( pow(vertices_x_y[C[k-1]][0] - vertices_x_y[j][0],2) 
                    //    + pow(vertices_x_y[C[k-1]][1] - vertices_x_y[j][1],2) );
                    if(d < distance[j]){
                        distance[j] = d;
                    }
                }
                for(int j=0;j<n;j++){
                    if(distance[j] > max_dist){
                        max_dist = distance[j];
                    }
                }
                if(max_dist < best_size){
                    best_size = max_dist;
                }
                if(max_dist <= (2*mid)*(2*mid)){
                    high = mid;
                }else{
                    low = mid;
                }
                iteration++;
                if(iteration > log2(n)){
                    stop_condition = true;
                }
            }
            //free(ptr_costs);
            printf("%f \n", sqrt(best_size));
        }
        
        // HS+
        if(strcmp(algorithm, "HS+") == 0){
            float high = greatest_cost;
            float low = 0;
            float mid;
            float d, d1, d2;
            srand(seed);
            float best_of_n;
            float distance[n];
            bool stop_condition = false;
            float best_size = +INFINITY;
            int iteration = 0;
            while(!stop_condition){
                mid = (high + low) / 2;
                best_of_n = +INFINITY;
                for(int iter=0;iter<n;iter++){
                    C[0] = iter;
                    for(int i=0;i<n;i++){
                        distance[i] = +INFINITY;
                    }
                    for(int i=1;i<k;i++){
                        // UPDATE DISTANCE
                        for(int j=0;j<n;j++){
                            d1 = vertices_x_y[C[i-1]][0] - vertices_x_y[j][0];
                            d2 = vertices_x_y[C[i-1]][1] - vertices_x_y[j][1];
                            d = d1*d1 + d2*d2;
                            //d = sqrt( pow(vertices_x_y[C[i-1]][0] - vertices_x_y[j][0],2) 
                            //    + pow(vertices_x_y[C[i-1]][1] - vertices_x_y[j][1],2) );
                            if(d < distance[j]){
                                distance[j] = d;
                            }
                        }
                        // SEARCH FOR NEXT VERTEX (2r)
                        int center = -1;
                        for(int j=0;j<n;j++){
                            if(distance[j] > (2*mid)*(2*mid)){
                                center = j;
                            }
                        }
                        if(center != -1){
                            C[i] = center;   
                        }else{
                            C[i] = rand() % n;
                        }
                    }
                    // RE-UPDATE DISTANCE
                    float max_dist = 0;
                    for(int j=0;j<n;j++){
                        d1 = vertices_x_y[C[k-1]][0] - vertices_x_y[j][0];
                        d2 = vertices_x_y[C[k-1]][1] - vertices_x_y[j][1];
                        d = d1*d1 + d2*d2;
                        //d = sqrt( pow(vertices_x_y[C[k-1]][0] - vertices_x_y[j][0],2) 
                        //    + pow(vertices_x_y[C[k-1]][1] - vertices_x_y[j][1],2) );
                        if(d < distance[j]){
                            distance[j] = d;
                        }
                    }
                    for(int j=0;j<n;j++){
                        if(distance[j] > max_dist){
                            max_dist = distance[j];
                        }
                    }
                    if(max_dist < best_of_n){
                        best_of_n = max_dist;
                    }
                }
                if(best_of_n < best_size){
                    best_size = best_of_n;
                }
                if(best_of_n <= (2*mid)*(2*mid)){
                    high = mid;
                }else{
                    low = mid;
                }
                iteration++;
                if(iteration > log2(n)){
                    stop_condition = true;
                }
            }
            //free(ptr_costs);
            printf("%f \n", sqrt(best_size));
        }

        // CDSh
        if(strcmp(algorithm, "CDSh") == 0){
            srand(seed);
            int farthest_vertex;
            float max_dist;
            int S[n];
            float distance[n];
            bool dominated[n];
            int score[n];
            float best_size = +INFINITY;
            float high = greatest_cost;
            float low = 0;
            float mid;
            float d, d1, d2;
            int iteration = 0;
            bool stop_condition = false;
            best_size = +INFINITY;
            while(!stop_condition){
                mid = (high + low) / 2;
                // INITIALIZATION
                for(int i=0;i<n;i++){
                    distance[i] = +INFINITY;
                    dominated[i] = false;
                    score[i] = 0;
                }
                for(int i=0;i<n;i++){
                    for(int j=0;j<n;j++){
                        d1 = vertices_x_y[i][0] - vertices_x_y[j][0];
                        d2 = vertices_x_y[i][1] - vertices_x_y[j][1];
                        //d = sqrt( d1*d1 + d2*d2 );
                        //if(i!=j & d <= mid){
                        d = d1*d1 + d2*d2;
                        if(i!=j & d <= mid*mid){
                            score[i] = score[i]+ 1;
                        }
                    }
                }
                for(int i=0;i<k;i++){
                    if(i>0){
                        // UPDATE DISTANCE
                        for(int j=0;j<n;j++){
                            d1 = vertices_x_y[C[i-1]][0] - vertices_x_y[j][0];
                            d2 = vertices_x_y[C[i-1]][1] - vertices_x_y[j][1];
                            //d = sqrt( d1*d1 + d2*d2 );
                            //if(d < distance[j]){
                            d = d1*d1 + d2*d2;
                            if(d < distance[j]){
                                distance[j] = d;
                            }
                        }
                        max_dist = 0;
                        for(int j=0;j<n;j++){
                            if(distance[j] > max_dist){
                                max_dist = distance[j];
                                farthest_vertex = j;
                            }
                        }
                    }else{
                        farthest_vertex = rand() % n;
                    }
                    // GET MAX SCORE CRITICAL NEIGHBOR
                    int max_score = -1;
                    int max_score_neighbor;
                    for(int j=0;j<n;j++){
                        d1 = vertices_x_y[farthest_vertex][0] - vertices_x_y[j][0];
                        d2 = vertices_x_y[farthest_vertex][1] - vertices_x_y[j][1];
                        //d = sqrt( d1*d1 + d2*d2 );
                        //if(d <= mid){
                        d = d1*d1 + d2*d2;
                        if(d <= mid*mid){
                            if(score[j] > max_score){
                                max_score = score[j];
                                max_score_neighbor = j;
                            }
                        }
                    }
                    C[i] = max_score_neighbor;
                    // UPDATE SCORE
                    int s_size = 0;
                    for(int j=0;j<n;j++){
                        d1 = vertices_x_y[C[i]][0] - vertices_x_y[j][0];
                        d2 = vertices_x_y[C[i]][1] - vertices_x_y[j][1];
                        //d = sqrt( d1*d1 + d2*d2 );
                        //if(d <= mid && dominated[j] == false){
                        d = d1*d1 + d2*d2;
                        if(d <= mid*mid && dominated[j] == false){
                            dominated[j] = true;
                            S[s_size] = j;
                            s_size++;
                        }
                    }
                    for(int j=0;j<s_size;j++){
                        for(int b=0;b<n;b++){
                            d1 = vertices_x_y[S[j]][0] - vertices_x_y[b][0];
                            d2 = vertices_x_y[S[j]][1] - vertices_x_y[b][1];
                            //d = sqrt( d1*d1 + d2*d2 );
                            //if(d <= mid && S[j] != b){
                            d = d1*d1 + d2*d2;
                            if(d <= mid*mid && S[j] != b){
                                score[b] = score[b] - 1;
                            }
                        }
                    }
                }
                // RE-UPDATE DISTANCE
                for(int j=0;j<n;j++){
                    d1 = vertices_x_y[C[k-1]][0] - vertices_x_y[j][0];
                    d2 = vertices_x_y[C[k-1]][1] - vertices_x_y[j][1];
                    //d = sqrt( d1*d1 + d2*d2 );
                    //if(d < distance[j]){
                    d = d1*d1 + d2*d2;
                    if(d < distance[j]){
                        distance[j] = d;
                    }
                }
                // GET MAX DIST
                max_dist = 0;
                for(int j=0;j<n;j++){
                    if(distance[j] > max_dist){
                        max_dist = distance[j];
                    }
                }
                if(max_dist < best_size){
                    best_size = max_dist;
                }
                if(best_size <= mid*mid){
                    high = mid;
                    //printf("((%f)) high \n" , mid);
                }else{
                    low = mid;
                    //printf("((%f)) low \n" , mid);
                }
                iteration++;
                if(iteration > log2(n)){
                    stop_condition = true;
                }
            }
            
            //free(costs);
            //ptr_costs = NULL;
            printf("%f \n", sqrt(best_size));
        }
        
        // CDSh+
        if(strcmp(algorithm, "CDSh+") == 0){
            srand(seed);
            int farthest_vertex;
            float max_dist;
            int S[n];
            float distance[n];
            bool dominated[n];
            int score[n];
            float high = greatest_cost;
            float low = 0;
            float mid;
            int iteration = 0;
            bool stop_condition = false;
            float overall_best_size = +INFINITY;
            while(!stop_condition){
                mid = (high + low) / 2;
                float best_size = +INFINITY;
                for(int d=0;d<n;d++){
                    // INITIALIZATION
                    for(int i=0;i<n;i++){
                        distance[i] = +INFINITY;
                        dominated[i] = false;
                        score[i] = 0;
                    }
                    for(int i=0;i<n;i++){
                        for(int j=0;j<n;j++){
                            d = sqrt( pow(vertices_x_y[i][0] - vertices_x_y[j][0],2) 
                                + pow(vertices_x_y[i][1] - vertices_x_y[j][1],2) );
                            if(i!=j & d <= mid){
                                score[i] = score[i]+ 1;
                            }
                        }
                    }
                    for(int i=0;i<k;i++){
                        if(i>0){
                            // UPDATE DISTANCE
                            for(int j=0;j<n;j++){
                                d = sqrt( pow(vertices_x_y[C[i-1]][0] - vertices_x_y[j][0],2) 
                                    + pow(vertices_x_y[C[i-1]][1] - vertices_x_y[j][1],2) );
                                if(d < distance[j]){
                                    distance[j] = d;
                                }
                            }
                            max_dist = 0;
                            for(int j=0;j<n;j++){
                                if(distance[j] > max_dist){
                                    max_dist = distance[j];
                                    farthest_vertex = j;
                                }
                            }
                        }else{
                            farthest_vertex = d;
                        }
                        // GET MAX SCORE CRITICAL NEIGHBOR
                        int max_score = -1;
                        int max_score_neighbor;
                        if(i > 0){
                            for(int j=0;j<n;j++){
                                d = sqrt( pow(vertices_x_y[farthest_vertex][0] - vertices_x_y[j][0],2) 
                                    + pow(vertices_x_y[farthest_vertex][1] - vertices_x_y[j][1],2) );
                                if(d <= mid){
                                    if(score[j] > max_score){
                                        max_score = score[j];
                                        max_score_neighbor = j;
                                    }
                                }
                            }     
                        }else{
                            max_score_neighbor = farthest_vertex;
                        }
                        C[i] = max_score_neighbor;
                        // UPDATE SCORE
                        int s_size = 0;
                        for(int j=0;j<n;j++){
                            d = sqrt( pow(vertices_x_y[C[i]][0] - vertices_x_y[j][0],2) 
                                + pow(vertices_x_y[C[i]][1] - vertices_x_y[j][1],2) );
                            if(d <= mid && dominated[j] == false){
                                dominated[j] = true;
                                S[s_size] = j;
                                s_size++;
                            }
                        }
                        for(int j=0;j<s_size;j++){
                            for(int b=0;b<n;b++){
                                d = sqrt( pow(vertices_x_y[S[j]][0] - vertices_x_y[b][0],2) 
                                    + pow(vertices_x_y[S[j]][1] - vertices_x_y[b][1],2) );
                                if(d <= mid && S[j] != b){
                                    score[b] = score[b] - 1;
                                }
                            }
                        }
                    }
                    // RE-UPDATE DISTANCE
                    for(int j=0;j<n;j++){
                        d = sqrt( pow(vertices_x_y[C[k-1]][0] - vertices_x_y[j][0],2) 
                            + pow(vertices_x_y[C[k-1]][1] - vertices_x_y[j][1],2) );
                        if(d < distance[j]){
                            distance[j] = d;
                        }
                    }
                    // GET MAX DIST
                    max_dist = 0;
                    for(int j=0;j<n;j++){
                        if(distance[j] > max_dist){
                            max_dist = distance[j];
                        }
                    }
                    if(max_dist < best_size){
                        best_size = max_dist;
                    }
                }
                if(best_size < overall_best_size){
                    overall_best_size = best_size;
                }
                if(overall_best_size <= mid){
                    high = mid;
                }else{
                    low = mid;
                }
                iteration++;
                if(iteration > log2(n)){
                    stop_condition = true;
                }
            }
            
            //free(costs);
            //ptr_costs = NULL;
            printf("%f \n", overall_best_size);
        }
        
        clock_t end = clock();
        double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
        total_time = total_time + time_spent;
        exec_time[instance] = time_spent;
    }
    printf("TIME SPENT PER INSTANCE \n");
    for(int i=0;i<4;i++){
        printf("%f \n", exec_time[i]);      
    }
    printf("TOTAL TIME: %f \n", total_time);
    printf("AVERAGE TIME: %f", total_time / 4);
    return (EXIT_SUCCESS);
}
